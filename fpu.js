(function ($) {

  Drupal.behaviors.FastPermissionsUpdateForm = {
    attach: function (context, settings) {
    
      $('#permissions').find('td').find('div.form-type-checkbox').each(function() {

        parentTD = $(this).parent('td');

        // Check there is a checkbox to the left
        if ($(parentTD).prev().find('div').hasClass('form-type-checkbox')) {
          // Create a span and add a left triangle
          lftSpn = document.createElement('span');
          $(lftSpn).append("&#9664;")
          $(this).prepend(lftSpn);

          // Add our event, check all  checkboxes to the left, unless they are checked, then uncheck them.
          $(lftSpn).click(function() {

            toCheckTD = $(this).parents('td');

            while (toCheckTD.find('input').length > 0) {
              tmpCheck = toCheckTD.find('input');

              if (tmpCheck.attr('checked')) {
                tmpCheck.removeAttr('checked')
              }
              else {
                tmpCheck.attr('checked', 'checked');
              }

              toCheckTD = toCheckTD.prev();
            }
          })
        }

        // Same as above, but to the right.
        if ($(this).parent('td').next().find('div').hasClass('form-type-checkbox')) {
          rgtSpn = document.createElement('span');
          $(rgtSpn).append("&#9654;")
          $(this).append(rgtSpn);

          $(rgtSpn).click(function() {

            toCheckTD = $(this).parents('td');

            while (toCheckTD.find('input').length > 0) {
              tmpCheck = toCheckTD.find('input');

              if (tmpCheck.attr('checked')) {
                tmpCheck.removeAttr('checked')
              }
              else {
                tmpCheck.attr('checked', 'checked');
              }

              toCheckTD = toCheckTD.next();
            }
          })
        }
      })
    }
  };

}(jQuery));